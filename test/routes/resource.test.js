const { resource } = require('../../src');

const handler = () => {};

test('resource with all methods', () => {
  expect(resource('app', { handler })).toEqual([
    { path: '/app', method: 'post', handler },
    { path: '/app', method: 'delete', handler },
    { path: '/app', method: 'get', handler },
    { path: '/app', method: 'put', handler },
  ]);
});

test('resource with no methods', () => {
  expect(resource('app', { only: [], handler })).toEqual([]);
});

test('resource with one method', () => {
  expect(resource('app', { handler, only: ['show'] })).toEqual([
    { path: '/app', method: 'get', handler },
  ]);
});

test('resource with some methods', () => {
  expect(resource('app', { handler, only: ['show', 'create'] })).toEqual([
    { path: '/app', method: 'post', handler },
    { path: '/app', method: 'get', handler },
  ]);
});
