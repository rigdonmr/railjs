const { drawRoutes } = require('../../src');

test('An empty array returns an empty array', () => {
  expect(drawRoutes([])).toEqual([]);
});

test('A flattened array returns itself', () => {
  expect(drawRoutes([1, 2, 3])).toEqual([1, 2, 3]);
});

test('A singularly nested array flattens', () => {
  expect(drawRoutes([1, 2, [3, 4, 5]])).toEqual([1, 2, 3, 4, 5]);
});

test('A multiply nested array flattens', () => {
  expect(drawRoutes([1, 2, [3, 4, [5, 6, 7]]])).toEqual([1, 2, 3, 4, 5, 6, 7]);
});
