const { resources } = require('../../src');

const handler = () => {};

test('resources with all methods', () => {
  expect(resources('apps', { handler })).toEqual([
    { path: '/apps', method: 'post', handler },
    { path: '/apps/:id', method: 'delete', handler },
    { path: '/apps', method: 'get', handler },
    { path: '/apps/:id', method: 'get', handler },
    { path: '/apps/:id', method: 'put', handler },
  ]);
});

test('resources with no methods', () => {
  expect(resources('apps', { only: [], handler })).toEqual([]);
});

test('resources with one method', () => {
  expect(resources('apps', { handler, only: ['show'] })).toEqual([
    { path: '/apps/:id', method: 'get', handler },
  ]);
});

test('resources with some methods', () => {
  expect(resources('apps', { handler, only: ['show', 'create'] })).toEqual([
    { path: '/apps', method: 'post', handler },
    { path: '/apps/:id', method: 'get', handler },
  ]);
});
