const { http } = require('../../src');
const { get, put, post, destroy } = http;

const handler = () => {};

test('http get', () => {
  expect(get('/apps', handler)).toEqual({ path: '/apps', method: 'get', handler });
});

test('http put', () => {
  expect(put('/apps/:id', handler)).toEqual({ path: '/apps/:id', method: 'put', handler });
});

test('http post', () => {
  expect(post('/apps', handler)).toEqual({ path: '/apps', method: 'post', handler });
});

test('http delete', () => {
  expect(destroy('/apps/:id', handler)).toEqual({ path: '/apps/:id', method: 'delete', handler });
});
