const { namespace, resources, resource, http } = require('../../src');
const { get, put, post, destroy } = http;

const handler = () => {};

test('namespace on vanilla routes', () => {
  expect(namespace('admin', [
    { path: '/foo', method: 'get', handler },
    { path: '/bar', method: 'put', handler },
  ])).toEqual([
    { path: '/admin/foo', method: 'get', handler },
    { path: '/admin/bar', method: 'put', handler },
  ]);
});

test('namespace on resources', () => {
  expect(namespace('admin', [
    resources('apps', { handler }),
    resources('comments', { only: ['index', 'update'], handler }),
  ])).toEqual([
    { path: '/admin/apps', method: 'post', handler },
    { path: '/admin/apps/:id', method: 'delete', handler },
    { path: '/admin/apps', method: 'get', handler },
    { path: '/admin/apps/:id', method: 'get', handler },
    { path: '/admin/apps/:id', method: 'put', handler },
    { path: '/admin/comments', method: 'get', handler },
    { path: '/admin/comments/:id', method: 'put', handler },
  ]);
});

test('namespace on resource', () => {
  expect(namespace('admin', [
    resource('app', { handler }),
    resource('comments', { only: ['show', 'update'], handler }),
  ])).toEqual([
    { path: '/admin/app', method: 'post', handler },
    { path: '/admin/app', method: 'delete', handler },
    { path: '/admin/app', method: 'get', handler },
    { path: '/admin/app', method: 'put', handler },
    { path: '/admin/comments', method: 'get', handler },
    { path: '/admin/comments', method: 'put', handler },
  ]);
});

test('namespace on http methods', () => {
  expect(namespace('admin', [
    get('/apps', handler),
    put('/apps', handler),
    post('/apps', handler),
    destroy('/apps', handler),
  ])).toEqual([
    { path: '/admin/apps', method: 'get', handler },
    { path: '/admin/apps', method: 'put', handler },
    { path: '/admin/apps', method: 'post', handler },
    { path: '/admin/apps', method: 'delete', handler },
  ]);
});
