const {
  drawRoutes,
  resource,
  resources,
  namespace,
} = require('jails');

module.exports = drawRoutes([
  resources('apps'),

  resource('console'),

  namespace('admin', [
    resources('catalogs', { only: ['show'] }),

    resource('settings', { only: ['show', 'update'] }),
  ]),
]);
