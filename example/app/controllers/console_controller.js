module.exports = {
  create(req, res) {
    res.json({ message: 'Successfully created console' });
  },

  destroy(req, res) {
    res.json({ message: `Successfully deleted console` });
  },

  show(req, res) {
    res.json({ message: `Successfully got console` });
  },

  update(req, res) {
    res.json({ message: `Successfully updated console` });
  },
};
