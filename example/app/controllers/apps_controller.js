module.exports = {
  create(req, res) {
    res.json({ message: 'Successfully created app' });
  },

  destroy(req, res) {
    const { id } = req.params;
    res.json({ message: `Successfully deleted app with id=${id}` });
  },

  index(req, res) {
    res.json({ message: 'Successfully got all apps' });
  },

  show(req, res) {
    const { id } = req.params;
    res.json({ message: `Successfully got app with id=${id}` });
  },

  update(req, res) {
    const { id } = req.params;
    res.json({ message: `Successfully updated app with id=${id}` });
  },
};
