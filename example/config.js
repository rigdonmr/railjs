// This file is used to start the application.

const express = require('express');
const app = express();
const routes = require('./config/routes');
const { useRoutes } = require('jails');

useRoutes(app, routes);

app.listen(3000, () => console.log('App listening on port 3000!'));
