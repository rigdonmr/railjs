const dateformat = require('dateformat');

function logger(req, res, next) {
  const {
    body,
    method,
    originalUrl,
    params,
    query,
  } = req;
  const timestamp = dateformat(Date.now(), ('yyyy-mm-dd HH:MM:ss o'));

  console.log();

  console.log(`Started ${method} "${originalUrl}" at ${timestamp}`);

  if (Object.keys(params).length > 0) {
    console.log('  Parameters:', params);
  }

  if (Object.keys(query).length > 0) {
    console.log('  Query:', query);
  }

  if (Object.keys(body).length > 0) {
    console.log('  Body:', body);
  }

  console.log();

  next();
}

module.exports = logger;
