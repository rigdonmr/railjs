const logger = require('../logger');
const { json } = require('body-parser');

function useRoutes(app, routes) {
  app.use(json());

  routes.forEach(route => {
    const { method, path, handler } = route;
    app[method](path, logger, handler);
  });
}

module.exports = useRoutes;
