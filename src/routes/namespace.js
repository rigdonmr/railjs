const { flattenDeep } = require('lodash/array');

function namespace(name, routes) {
  return flattenDeep(routes).map(route => Object.assign({}, route, {
    path: `/${name}${route.path}`,
  }));
}

module.exports = namespace;
