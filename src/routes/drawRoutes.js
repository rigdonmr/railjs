const { flattenDeep } = require('lodash/array');

function drawRoutes(routes) {
  return flattenDeep(routes);
}

module.exports = drawRoutes;
