module.exports = {
  drawRoutes: require('./drawRoutes'),
  http: require('./http'),
  namespace: require('./namespace'),
  resource: require('./resource'),
  resources: require('./resources'),
  useRoutes: require('./useRoutes'),
};
