function get(path, handler) {
  return { method: 'get', path, handler };
}

function put(path, handler) {
  return { method: 'put', path, handler };
}

function post(path, handler) {
  return { method: 'post', path, handler };
}

function destroy(path, handler) {
  return { method: 'delete', path, handler };
}

module.exports = { get, put, post, destroy };
