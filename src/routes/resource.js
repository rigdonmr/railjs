const path = require('path');
const base = path.dirname(require.main.filename);

function Options() {
  this.only = ['create', 'destroy', 'show', 'update'];
  this.handler = null;
}

function resource(
  name,
  options = {}
) {
  const fullOptions = Object.assign({}, new Options(), options);

  let controller = undefined;
  if (!options.handler) {
    controller = require(`${base}/app/controllers/${name}_controller`);
  }

  const routes = [];

  if (fullOptions.only.includes('create')) {
    routes.push({
      method: 'post',
      path: `/${name}`,
      handler: fullOptions.handler || controller.create,
    });
  }

  if (fullOptions.only.includes('destroy')) {
    routes.push({
      method: 'delete',
      path: `/${name}`,
      handler: fullOptions.handler || controller.destroy,
    });
  }

  if (fullOptions.only.includes('show')) {
    routes.push({
      method: 'get',
      path: `/${name}`,
      handler: fullOptions.handler || controller.show,
    });
  }

  if (fullOptions.only.includes('update')) {
    routes.push({
      method: 'put',
      path: `/${name}`,
      handler: fullOptions.handler || controller.update,
    });
  }

  return routes;
}

module.exports = resource;
